var Samsara = require('samsarajs');

var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;

var View = Samsara.Core.View;

module.exports = View.extend( {
  defaults : {
    title: 'About',
    info: 'This future fab app is what on demand product design (api) and customization might look like in the future. The code libraries used to make this app were developed by Reza Ali as part of the Guest Research Project V3 at YCAM Interlab.',
    popup : {
      backgroundColor: 'rgba( 255, 255, 255, 0.075 )'
    },
    dimmer: {
      backgroundColor: 'rgba( 0, 0, 0, 0.85 )'
    }
  },
  events : {
    open : "onOpen",
    close : "onClose",
  },
  initialize : function( options ) {
    this.easing = { curve: 'spring', damping: 0.75, period: 75, velocity: 0.0 };
    this.openState = new Transitionable( 0 );

    var translation = this.openState.map( function( value ) { return Transform.translateY( ( value - 1 ) * options.height ); } );
    var opacity = this.openState.map( function( value ) { return value; } );

    var overlay = new ContainerSurface( {
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 1.0 ]
    } );

    var dimmer = new Surface( {
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 1.0 ],
      properties: {
        backgroundColor: options.dimmer.backgroundColor,
        zIndex: -1.0
      }
    } );

    dimmer.on( 'click', ( function() { this.emit( 'info' ); } ).bind( this ) );

    var popup = new ContainerSurface( {
      origin: [ 0.5, 0.5 ],
      size: [ 400, 36 * 8 ],
      properties: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: '36px',
        lineHeight: '72px',
        fontWeight: 200,
        color: 'rgba( 255, 255, 255, 0.75 )',
        boxShadow: '0px 4px 8px rgba( 0, 0, 0, 0.125 )',
        backgroundColor: options.popup.backgroundColor
      }
    } );

    var title = new Surface( {
      content: options.title,
      origin: [ 0.5, 0.0 ],
      proportions: [ 1.0, 0.25 ],
      properties: {
        fontWeight: 300,
        color: 'rgba( 255, 255, 255, 0.75 )',
        borderBottom: '1px solid rgba( 255, 255, 255, 0.05 )',
      },
      classes: ['noselect']
    } );

    var info = new Surface( {
      content: options.info,
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 0.5 ],
      margins: [ 16, 14 ],
      properties: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: '14px',
        lineHeight: '28px',
        fontWeight: 200,
        color: 'rgba( 255, 255, 255, 0.75 )',
        overflow: 'hidden'
        // backgroundColor: 'rgba( 255, 0, 0, 0.1 )',
      },
      classes: ['selection']
    } );

    var rezaContent = 'REZA';
    var ycamContent = 'YCAM';

    var contents = [ {
        emit: 'reza',
        content: rezaContent,
      },
      {
        emit: 'ycam',
        content: ycamContent,
      }
    ];

    var icons = new ContainerSurface( {
      origin: [ 0.5, 1.0 ],
      proportions: [ 1.0, 0.25 ],
      properties: {
        borderTop: '1px solid rgba( 255, 255, 255, 0.05 )',
        cursor: 'pointer',
      }
    } );

    function createLogo( content, emit ) {
      var cont = new ContainerSurface( { proportions: [ 1.0 / contents.length, 1.0 ] } );
      var surf = new Surface( {
        content: content,
        properties: {
          zIndex: -1.0
        } } );
      var hitArea = new Surface( { properties: { zIndex: 1.0 } } );
      var surfOpacity = new Transitionable( 0.5 );
      var surfEasing = { curve: 'easeInOutCubic', duration: 200 };

      hitArea.on( 'mouseover', function() { surfOpacity.set( 1.0, surfEasing ); } );
      hitArea.on( 'mouseout', function() { surfOpacity.set( 0.5, surfEasing ); } );
      hitArea.on( 'click', ( function() { this.emit( emit ); surfOpacity.set( 0.5, surfEasing ); } ).bind( this ) );

      cont.add( { align: [ 0.0, 0.0 ], opacity: surfOpacity } ).add( surf );
      cont.add( { align: [ 0.0, 0.0 ] } ).add( hitArea );
      return cont;
    }

    function createSpacer() {
      return new Surface( {
        origin: [ 0.5, 0.5 ],
        size: [ 1, undefined ],
        proportions: [ undefined, 0.7 ],
        properties: {
          backgroundColor: 'rgba( 255, 255, 255, 0.05 )'
        }
      } );
    }

    var inc = 1.0 / contents.length;
    for( var i = 0; i < contents.length; i++ ) {
      var cur = contents[ i ];
      icons.add( { align: [ i * inc, 0.0 ] } ).add( createLogo.call( this, cur.content, cur.emit ) );
      if( i != 0 ) {
        icons.add( { align: [ i * inc, 0.5 ] } ).add( createSpacer() );
      }
    }

    popup.add( { align: [ 0.5, 0.0 ] } ).add( title );
    popup.add( { align: [ 0.5, 0.5 ] } ).add( info );
    popup.add( { align: [ 0.5, 1.0 ] } ).add( icons );

    overlay.add( { align: [ 0.5, 0.5 ], transform: Transform.translateZ( 1 ) } ).add( popup );
    overlay.add( { align: [ 0.5, 0.5 ] } ).add( dimmer );
    this.add( { align: [ 0.5, 0.5 ], opacity: opacity } ).add( overlay );
  },
  onOpen : function( data ) {
    this.openState.set( 1.0, this.easing );
  },
  onClose : function( data ) {
    this.openState.set( 0.0, this.easing );
  }
} );
