var Samsara = require('samsarajs');

var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;

var View = Samsara.Core.View;

module.exports = View.extend( {
  defaults : {
    title: 'Share Your Design',
    popup : {
      backgroundColor: 'rgba( 255, 255, 255, 0.075 )'
    },
    dimmer: {
      backgroundColor: 'rgba( 0, 0, 0, 0.85 )'
    }
  },
  events : {
    open : "onOpen",
    close : "onClose",
  },
  initialize : function( options ) {
    this.easing = { curve: 'spring', damping: 0.75, period: 75, velocity: 0.0 };
    this.openState = new Transitionable( 0 );

    var translation = this.openState.map( function( value ) { return Transform.translateY( ( value - 1 ) * options.height ); } );
    var opacity = this.openState.map( function( value ) { return value; } );

    var overlay = new ContainerSurface( {
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 1.0 ]
    } );

    var dimmer = new Surface( {
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 1.0 ],
      properties: {
        backgroundColor: options.dimmer.backgroundColor,
        zIndex: -1.0
      }
    } );

    dimmer.on( 'click', ( function() { this.emit( 'share' ); } ).bind( this ) );

    var popup = new ContainerSurface( {
      origin: [ 0.5, 0.5 ],
      size: [ 400, 36 * 6 ],
      properties: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: '24px',
        lineHeight: '72px',
        fontWeight: 200,
        color: 'rgba( 255, 255, 255, 0.75 )',
        boxShadow: '0px 4px 8px rgba( 0, 0, 0, 0.125 )',
        backgroundColor: options.popup.backgroundColor
      }
    } );

    var title = new Surface( {
      content: options.title,
      origin: [ 0.5, 0.0 ],
      proportions: [ 1.0, 1.0 / 3.0 ],
      properties: {
        fontSize: '32px',
        color: 'rgba( 255, 255, 255, 0.75 )',
        borderBottom: '1px solid rgba( 255, 255, 255, 0.05 )',
      },
      classes: ['noselect']
    } );

    this.link = new Surface( {
      content: '',
      origin: [ 0.5, 0.5 ],
      proportions: [ 1.0, 1.0 / 3.0 ],
      properties: {
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontSize: '22px',
        fontWeight: 200,
        color: 'rgba( 255, 255, 255, 0.75 )',
        borderBottom: '1px solid rgba( 255, 255, 255, 0.05 )',
      },
      classes: ['selection']
    } );

    var iconSize = 36;
    var iconColor = '<path fill="rgba(255,255,255,1.0)" ';
    var svgPrefix = '<svg style="width:'+iconSize+'px;height:'+iconSize+'px" viewBox="0 0 24 24">';

    var pinterestContent = svgPrefix;
    pinterestContent += iconColor + 'd="M13.25,17.25C12.25,17.25 11.29,16.82 10.6,16.1L9.41,20.1L9.33,20.36L9.29,20.34C9.04,20.75 8.61,21 8.12,21C7.37,21 6.75,20.38 6.75,19.62C6.75,19.56 6.76,19.5 6.77,19.44L6.75,19.43L6.81,19.21L9.12,12.26C9.12,12.26 8.87,11.5 8.87,10.42C8.87,8.27 10.03,7.62 10.95,7.62C11.88,7.62 12.73,7.95 12.73,9.26C12.73,10.94 11.61,11.8 11.61,13C11.61,13.94 12.37,14.69 13.29,14.69C16.21,14.69 17.25,12.5 17.25,10.44C17.25,7.71 14.89,5.5 12,5.5C9.1,5.5 6.75,7.71 6.75,10.44C6.75,11.28 7,12.12 7.43,12.85C7.54,13.05 7.6,13.27 7.6,13.5A1.25,1.25 0 0,1 6.35,14.75C5.91,14.75 5.5,14.5 5.27,14.13C4.6,13 4.25,11.73 4.25,10.44C4.25,6.33 7.73,3 12,3C16.27,3 19.75,6.33 19.75,10.44C19.75,13.72 17.71,17.25 13.25,17.25Z" /></svg>';

    var twitterContent = svgPrefix;
    twitterContent += iconColor + 'd="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z" /></svg>'

    var facebookContent = svgPrefix;
    facebookContent += iconColor + 'd="M17,2V2H17V6H15C14.31,6 14,6.81 14,7.5V10H14L17,10V14H14V22H10V14H7V10H10V6A4,4 0 0,1 14,2H17Z" /></svg>';

    var contents = [  {
        emit: 'facebook',
        content: facebookContent,
      },
      {
        emit: 'twitter',
        content: twitterContent,
      },
      {
        emit: 'pinterest',
        content: pinterestContent,
      }
    ];

    var icons = new ContainerSurface( {
      origin: [ 0.5, 1.0 ],
      proportions: [ 1.0, 1.0 / 3.0 ],
      properties: {
        cursor: 'pointer',
      }
    } );

    function createSocialIcon( content, emit ) {
      var cont = new ContainerSurface( { proportions: [ 1.0 / contents.length, 1.0 ] } );
      var surf = new Surface( { content: content, properties: { zIndex: -1.0 } } );
      var hitArea = new Surface( { properties: { zIndex: 1.0 } } );
      var surfOpacity = new Transitionable( 0.25 );
      var surfEasing = { curve: 'easeInOutCubic', duration: 200 };

      hitArea.on( 'mouseover', function() { surfOpacity.set( 0.75, surfEasing ); } );
      hitArea.on( 'mouseout', function() { surfOpacity.set( 0.25, surfEasing ); } );
      hitArea.on( 'click', ( function() { this.emit( emit, { url: this.link.getContent() } ); surfOpacity.set( 0.25, surfEasing ); } ).bind( this ) );

      cont.add( { align: [ 0.0, 0.0 ], opacity: surfOpacity } ).add( surf );
      cont.add( { align: [ 0.0, 0.0 ] } ).add( hitArea );
      return cont;
    }

    function createSpacer() {
      return new Surface( {
        origin: [ 0.5, 0.5 ],
        size: [ 1, undefined ],
        proportions: [ undefined, 0.7 ],
        properties: {
          backgroundColor: 'rgba( 255, 255, 255, 0.05 )',
          zIndex: 1
        }
      } );
    }

    var inc = 1.0 / contents.length;
    for( var i = 0; i < contents.length; i++ ) {
      var cur = contents[ i ];
      icons.add( { align: [ i * inc, 0.0 ] } ).add( createSocialIcon.call( this, cur.content, cur.emit ) );
      if( i != 0 ) {
        icons.add( { align: [ i * inc, 0.5 ] } ).add( createSpacer() );
      }
    }

    popup.add( { align: [ 0.5, 0.0 ] } ).add( title );
    popup.add( { align: [ 0.5, 0.5 ] } ).add( this.link );
    popup.add( { align: [ 0.5, 1.0 ] } ).add( icons );

    overlay.add( { align: [ 0.5, 0.5 ], transform: Transform.translateZ( 1 ) } ).add( popup );
    overlay.add( { align: [ 0.5, 0.5 ], transform: Transform.translateZ( -1 ) } ).add( dimmer );
    this.add( { align: [ 0.5, 0.5 ], opacity: opacity } ).add( overlay );
  },
  onOpen : function( data ) {
    this.link.setContent( 'loading' );
    getShortUrl( encodeURIComponent( window.location.href ), ( function( result ) {
      this.link.setContent( JSON.parse( result.response ).data.url );
    } ).bind( this ) );
    this.openState.set( 1.0, this.easing );
  },
  onClose : function( data ) {
    this.openState.set( 0.0, this.easing );
  }
} );


function getShortUrl( url, callback ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() { if( xhttp.readyState == 4 && xhttp.status == 200 ) { callback( xhttp ); } };
  xhttp.open( 'GET', 'https://api-ssl.bitly.com/v3/shorten?access_token=98842cc798a45f2b72b7173b271fbeb8e9266853&longUrl=' + url, true );
  xhttp.send();
}
