var Samsara = require('samsarajs');

var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;

var View = Samsara.Core.View;

var createSpacer = require('./CreateSpacer');

module.exports = View.extend( {
  defaults : {
    title: 'Title',
    height: 56,
    backgroundColor: 'rgba( 0, 0, 0, 0.5 )'
  },
  initialize : function( options ) {
    var t = new Transitionable( 0 );
    var translation = t.map( function( value ) { return Transform.translateY( ( value - 1 ) * options.height ); } );
    var opacity = t.map( function( value ) { return value; } );

    var backgroundBar = new ContainerSurface( {
      size: [ undefined, options.height ],
      properties: {
        backgroundColor: options.backgroundColor
      }
    } );

    var title = new Surface( {
      content: options.title,
      size : [ true, options.height ],
      properties: {
        fontSize: '24px',
        fontWeight: 200,
        color: 'white',
        fontFamily: 'Roboto',
        lineHeight: options.height + 'px'
      },
      classes: [ 'noselect' ]
    });

    var iconProperties = {
      color: 'white',
      fontSize: '20px',
      textAlign: 'center',
      lineHeight: options.height + 'px'
    };

    function makeIcon( type, emit, that ) {
      var iconType = '<button class="mdl-button mdl-js-button mdl-button--icon"><i class="material-icons">'
      iconType += type;
      iconType += '</i></button>';
      var iconSurface = new Surface( {
        content: iconType,
        size: [ options.height, options.height ],
        origin : [ 1, 0 ],
        properties: iconProperties,
        classes: [ 'noselect' ]
      } );
      iconSurface.on( 'click', ( function() {
        this.emit( emit );
      } ).bind( that ) );
      return iconSurface;
    }

    var iconContainer = new ContainerSurface( { origin: [ 1.0, 0.0 ] } );
    var icons = [
      makeIcon( 'info_outline', 'info', this ),
      makeIcon( 'share', 'share', this ),
      makeIcon( 'file_download', 'save', this ),
      makeIcon( 'tune', 'tune', this )
    ];
    var len = icons.length;
    for( var i = 0; i < len; i++ ) {
      var placement = { align: [ 1.0, 0.0 ], transform: Transform.translateX( -options.height * i )  };
      iconContainer.add( placement ).add( icons[ i ] );
      if( i != 0 && i != len ) {
        iconContainer.add( placement ).add( createSpacer() );
      }
    }

    backgroundBar.add( { transform: Transform.translateX( 16 ) } ).add( title );
    backgroundBar.add( { align: [ 1.0, 0.0 ] } ).add( iconContainer );
    this.add( { transform: translation, opacity: opacity } ).add( backgroundBar );
    t.set( 1.0, { curve: 'spring', damping: 0.75, period: 100, velocity: -0.2 } );
  }
} );
