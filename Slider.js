var Samsara = require('samsarajs');

var GenericInput = Samsara.Inputs.GenericInput;
var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;
var View = Samsara.Core.View;

var map = require('mhf').map;

module.exports = View.extend( {
  defaults : {
    param: undefined,
    height: 56,
    width: 256,
    label: {
      color: 'rgba( 255, 255, 255, 0.75 )',
    },
    head: {
      color: 'white',
    },
    track: {
      highlightColor: 'white',
      backgroundColor: 'rgba( 255, 255, 255, 0.2 )',
    },
    backgroundColor: 'rgba( 255, 255, 255, 0.1 )'
  },
  initialize : function( options ) {
    this.param = options.param;
    this.subscribe( this.param );

    var easing = { curve: 'easeOutCubic', duration: 200 };
    var value = new Transitionable( map( this.param.value, this.param.min, this.param.max, 0.0, 1.0 ) );
    var hover = new Transitionable( 0 );

    var headSize = hover.map( function( data ) {
      var nv = 1.0 + 0.5 * data;
      return Transform.scale( [ nv, nv, nv ] );
    } );

    var headAlign = value.map( function( data ) {
      return [ data, 0.33 ];
    } );

    var container = new ContainerSurface( {
      origin: [ 0.5, 0.0 ],
      size: [ options.width, options.height ],
      properties: {
        cursor: 'pointer',
        // backgroundColor: 'rgba( 255, 255, 255, 0.1 )',
      }
    } );

    var hitScale = 1.1;
    var hitArea = new Surface( {
      origin: [ 0.5, 0.0 ],
      proportions: [ hitScale, 1.0 ],
      properties: {
        // backgroundColor: 'rgba( 255, 0, 0, 0.1 )',
        zIndex: 2.0
      }
    } );

    var head = new Surface( {
      origin: [ 0.5, 0.5 ],
      size: [ 12, 12 ],
      aspectRatio: 1.0,
      properties: {
        backgroundColor: options.head.color,
        borderRadius: '36px',
        zIndex: -1
      }
    } );

    var trackHighlight = new Surface( {
      origin: [ 0.0, 0.5 ],
      size: [ undefined, 2 ],
      proportions: value.map( function( value ) {
        return [ value, undefined ];
      } ),
      properties: {
        backgroundColor: options.track.highlightColor,
        zIndex: -1
      },
    } );

    var track = new Surface( {
      origin: [ 0.5, 0.5 ],
      size: [ undefined, 2 ],
      properties: {
        backgroundColor: options.track.backgroundColor,
        zIndex: -2
      }
    } );

    var label = new Surface( {
      content: this.param.key + ': ' + this.param.value.toFixed( 3 ),
      origin: [ 0.0, 0.5 ],
      size: [ true, true ],
      properties: {
        fontFamily: 'Roboto',
        fontSize: 0.225 * options.height + 'px',
        lineHeight: 0.225 * options.height + 'px',
        fontWeight: 200,
        color: options.label.color,
        cursor: 'pointer',
        // backgroundColor: 'red',
        zIndex: -3
      }
    } );

    container.add( { align: [ 0.0, 0.66 ] } ).add( label );
    container.add( { align: [ 0.5, 0.0 ] } ).add( hitArea );
    container.add( { align: [ 0.5, 0.33 ] } ).add( track );
    container.add( { align: [ 0.0, 0.33 ] } ).add( trackHighlight );
    container.add( { align: headAlign, transform: headSize } ).add( head );

    var insideHitArea = false;
    var inside = function() { insideHitArea = true; };
    var outside = function() { insideHitArea = false; hover.set( 0.0, easing ); };

    hitArea.on( 'mouseover', inside );
    hitArea.on( 'mouseout', outside );

    hitArea.on( 'touchdown', inside );
    hitArea.on( 'touchup', outside );

    var mouseTouchInput = new GenericInput( [ 'mouse', 'touch' ] );
    mouseTouchInput.subscribe( hitArea );

    var offset = options.width * ( hitScale - 1.0 ) * 0.5;
    function calculateValue( input ) {
      if( input < offset ) { return 0.0; }
      else if( input > ( offset + options.width ) ) { return 1.0; }
      else { return ( input - offset ) / options.width; }
    }

    this.input.on( 'onChange', function( data ) {
      value.set( Math.max( 0.0, Math.min( 1.0, map( data.value, this.param.min, this.param.max, 0.0, 1.0 ) ) ) );
      label.setContent( this.param.key + ': ' + this.param.value.toFixed( 3 ) )
    } );

    function setValue( input ) {
      var max = this.param.max;
      var min = this.param.min;
      var step = this.param.step;
      var nv = map( input, 0.0, 1.0, min, max );
      if( step ) { nv = Math.round( nv / step ) * step; }
      value.set( map( nv, min, max, 0.0, 1.0 ) );
      this.param.setValue( map( input, 0.0, 1.0, min, max ) );
    }

    mouseTouchInput.on( 'start', ( function( data ) {
      if( insideHitArea ) {
        hover.set( 1.0, easing );
        setValue.call( this, calculateValue( data.offsetX ) );
        this.emit( 'start', { key: this.param.key, value: this.param.getValue() } );
      }
    } ).bind( this ) );

    mouseTouchInput.on( 'update', ( function( data ){
      if( insideHitArea ) {
        hover.set( 1.0, easing );
        setValue.call( this, calculateValue( data.offsetX ) );
        this.emit( 'update', { key: this.param.key, value: this.param.getValue() } );
      }
    } ).bind( this ) );

    mouseTouchInput.on( 'end', ( function( data ){
      if( insideHitArea ) {
        hover.set( 0.0, easing );
        setValue.call( this, calculateValue( data.offsetX ) );
        this.emit( 'end', { key: this.param.key, value: this.param.getValue() } );
      }
    } ).bind( this ) );
    this.add( container );
  }
} );
