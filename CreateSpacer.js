var Samsara = require('samsarajs');

var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;

module.exports = function( height ) {
  var container = new ContainerSurface( {
    origin: [ 0.0, 0.0 ],
    size: [ 1.0, height ]
  } );

  var surface = new Surface({
    origin: [ 0.5, 0.5 ],
    proportions: [ 0.5, 0.7 ],
    properties: {
      backgroundColor: 'rgba( 255, 255, 255, 0.2 )'
    },
  });
  container.add( { align: [ 0.5, 0.5 ] } ).add( surface );
  return container;
};
