var Samsara = require('samsarajs');

var GenericInput = Samsara.Inputs.GenericInput;
var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;
var View = Samsara.Core.View;

module.exports = View.extend( {
  defaults : {
    param: undefined,
    label: {
        content: 'Label',
        color: 'rgba( 255, 255, 255, 0.75 )',
    },
    hit: {
      color: 'rgba( 255, 255, 255, 0.75 )',
    },
    hitSize: 16,
    height: 56,
    width: 256,
    backgroundColor: 'rgba( 255, 255, 255, 0.10 )'
  },
  initialize : function( options ) {
    this.param = options.param;
    this.subscribe( this.param );

    var easing = { curve: 'easeOutCubic', duration: 200 };
    var value = new Transitionable( this.param.value ? 1.0 : 0.0 );
    var hover = new Transitionable( 0 );

    var container = new ContainerSurface( {
      origin: [ 0.5, 0.0 ],
      size: [ options.width, options.height ]
    } );

    var size = value.map( function( value ) {
      return [ options.hitSize, options.hitSize ];
    } );

    var scale = value.map( function( value ) {
      return Transform.scale( [ value, value, value ] );
    } );

    var hitArea = new ContainerSurface( {
      origin: [ 0.0, 0.5 ],
      properties: {
        cursor: 'pointer'
      },
      classes: [ 'noselect' ]
    } );

    var hitOutline = new ContainerSurface( {
      origin: [ 0.0, 0.5 ],
      size: size,
      properties: {
        borderRadius: options.hitSize + 'px',
        border: '1.5px solid ' + options.hit.color,
        cursor: 'pointer'
      },
      classes: [ 'noselect' ]
    } );

    var hitFill = new Surface( {
      origin: [ 0.5, 0.5 ],
      size: [ options.hitSize - 4, options.hitSize - 4 ],
      properties: {
        borderRadius: options.hitSize + 'px',
        backgroundColor: 'white'
      }
    } );

    var label = new Surface( {
      content: options.label.content,
      origin: [ 0.0, 0.5 ],
      size: [ true, options.hitSize ],
      properties: {
        fontFamily: 'Roboto',
        fontSize: options.hitSize + 'px',
        lineHeight: options.hitSize + 'px',
        fontWeight: 200,
        color: options.label.color,
        cursor: 'pointer'
      }
    } );

    hitOutline.add( { align: [ 0.5, 0.5 ], transform: scale, opacity: value } ).add( hitFill );
    container.add( { align: [ 0.0, 0.5 ] } ).add( hitOutline );
    container.add( { align: [ 0.0, 0.5 ], transform: Transform.translateX( options.hitSize * 1.5 ) } ).add( label );
    container.add( { align: [ 0.0, 0.5 ] } ).add( hitArea );
    this.add( container );

    this.input.on( 'onChange', function( data ) {
      value.set( data.value ? 1.0 : 0.0, easing );
    } );

    var mouseTouchInput = new GenericInput( [ 'mouse', 'touch' ] );
    mouseTouchInput.subscribe( hitArea );

    mouseTouchInput.on( 'start', toggleValue.bind( this ) );

    function toggleValue() {
      this.param.setValue( !this.param.getValue() );
      value.set( this.param.getValue() ? 1.0 : 0.0, easing );
      this.emit( 'end', { key: this.param.key, value: this.param.getValue() } );
    }
  }
} );
