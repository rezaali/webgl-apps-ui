var Samsara = require('samsarajs');
var EventHandler = Samsara.Events.EventHandler;

function Param( props ) {
  EventHandler.apply( this, arguments );
  this.key = props.key;
  this.value = props.value;
  this.defaultValue = this.value;
  this.min = props.min;
  this.max = props.max;
  this.step = props.step;
  this.type = props.type ? props.type : typeof this.value;
}

Param.prototype = Object.create( EventHandler.prototype );
Param.prototype.constructor = Param;

Param.prototype.setValue = function( value ) {
  var type = this.type;
  if( type === 'number' ) {
    var max = this.max;
    var min = this.min;
    var step = this.step;
    if( step ) {
      value = Math.round( value / step ) * step;
    }
    value = Math.max( min, Math.min( max, value ) );
    if( this.value !== value ) {
      this.value = value;
      this.emit( 'onChange', { key: this.key, value: this.value } );
    }
  }
  else {
    this.value = value;
    this.emit( 'onChange', { key: this.key, value: this.value } );
  }
};

Param.prototype.getValue = function() {
  return this.value;
};

Param.prototype.reset = function() {
  this.value = this.defaultValue;
  this.emit( 'onChange', { key: this.key, value: this.value } );
};

module.exports = Param;
