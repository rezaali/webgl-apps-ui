var Samsara = require('samsarajs');

var View = Samsara.Core.View;
var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var EventHandler = Samsara.Events.EventHandler;

var Surface = Samsara.DOM.Surface;
var Context = Samsara.DOM.Context;

var MouseInput = Samsara.Inputs.MouseInput;
var TouchInput = Samsara.Inputs.TouchInput;
var GenericInput = Samsara.Inputs.GenericInput;

GenericInput.register( { mouse : MouseInput, touch : TouchInput } );

import App from './../src/app';
document.title = App.getTitle();

loadAppParams( App.getParams(), getUrlParams() );

var TopBar = require('./TopBar');
var SaveBar = require('./SaveBar');
var SharePanel = require('./SharePanel');
var InfoPanel = require('./InfoPanel');
var ControlPanel = require('./ControlPanel');

var topBar = new TopBar( { title: App.getTitle() } );
var saveBar = new SaveBar();
var infoPanel = new InfoPanel();
var sharePanel = new SharePanel();
var controlPanel = new ControlPanel( {
  params: App.getParams(),
} );

var context = new Context();
context.add( topBar );
context.add( saveBar );
context.add( controlPanel );
context.add( sharePanel );
context.add( infoPanel );
context.mount( document.body );

var saveBarOpen = false;
var controlPanelOpen = false;
var sharePanelOpen = false;
var infoPanelOpen = false;
var fileName = '';

var logic = new EventHandler();
logic.subscribe( topBar );
logic.subscribe( saveBar );
logic.subscribe( sharePanel );
logic.subscribe( infoPanel );
logic.subscribe( controlPanel );

logic.on( 'save', function() {
  saveBarOpen = !saveBarOpen;
  saveBar.trigger( saveBarOpen ? 'open' : 'close' );
  controlPanelOpen = false;
  controlPanel.trigger( 'close' );
} );

logic.on( 'tune', function() {
  saveBarOpen = false;
  saveBar.trigger( 'close' );
  controlPanelOpen = !controlPanelOpen;
  controlPanel.trigger( controlPanelOpen ? 'open' : 'close' );
} );

logic.on( 'info', function() {
  saveBarOpen = false;
  saveBar.trigger( saveBarOpen ? 'open' : 'close' );
  controlPanelOpen = false;
  controlPanel.trigger( 'close' );
  infoPanelOpen = !infoPanelOpen;
  infoPanel.trigger( infoPanelOpen ? 'open' : 'close' );
} );

logic.on( 'share', function( data ) {
  saveBarOpen = false;
  saveBar.trigger( 'close' );
  controlPanelOpen = false;
  controlPanel.trigger( 'close' );
  sharePanelOpen = !sharePanelOpen;
  sharePanel.trigger( sharePanelOpen ? 'open' : 'close' );
} );

logic.on( 'png', function( data ) {
  App.savePng( filename );
} );

logic.on( 'stl', function( data ) {
  App.saveStl( filename );
} );

logic.on( 'obj', function( data ) {
  App.saveObj( filename );
} );

logic.on( 'data', function( data ) {
  saveAppParams( App.getParams() );
} );

logic.on( 'reza', function( data ) {
  window.open( 'http://www.syedrezaali.com', '_blank' );
} );

logic.on( 'ycam', function( data ) {
  window.open( 'http://www.ycam.jp/en', '_blank' );
} );

logic.on( 'facebook', function( data ) {
  var text = 'Check out this 3D object I designed with'
  var url = data.url;
  window.open(
    'http://www.facebook.com/sharer.php?s=100&p[title]=' + text + '&' +
    'p[url]=' + url,
    'sharer', getShareOptions() );
} );

logic.on( 'twitter', function( data ) {
  var text = '3D Printable object designed online, customize it here:'
  window.open(
    'https://twitter.com/intent/tweet?text=' + encodeURIComponent( text ) +  '&' +
    'url=' + data.url + '&' +
    'via=rezaali&' +
    'hashtags=3DPrinting,DigitalFab,WebApp,3DP'
    ,'sharer', getShareOptions() );
} );

logic.on( 'pinterest', function( data ) {
  var description = 'Customizable 3D Printable object designed in the browser, download / customize it here: ' + data.url;
  var xhttp = new XMLHttpRequest();
  var fd = new FormData();
  fd.append( 'image', dataURItoBlob( App.getPng() ) );
  fd.append( 'description', '3D printable object designed with ' + data.url );
  xhttp.open( 'POST', 'https://api.imgur.com/3/image' );
  xhttp.setRequestHeader( 'Authorization', 'Client-ID 4833dd311bdf547');
  xhttp.onreadystatechange = function ( data ) {
    if( xhttp.status === 200 && xhttp.readyState === 4 ) {
      var res = JSON.parse( xhttp.responseText );
      var parser = document.createElement( 'a' );
      parser.href = res.data.link;
      var newUrl = 'https://' + parser.hostname + parser.pathname;
      window.open(
        'http://pinterest.com/pin/create/link/?url=' + encodeURIComponent( window.location.href ) + '&' +
        'media=' + newUrl + '&' +
        'description=' + encodeURIComponent( description )
        ,'sharer', getShareOptions() );
    }
  };
  xhttp.send( fd );
} );

logic.trigger( 'tune' );

function loadAppParams( params, urlParams ) {
  for( var i = 0; i < params.length; i++ ) {
    var param =  params[ i ];
    var key = decodeURIComponent( param.key );
    var urlParam = urlParams[ key ];
    if( urlParam != undefined ) {
      if( param.type == 'number' ) {
        param.setValue( parseFloat( urlParam ) );
      }
      else if( param.type == 'boolean' ) {
        param.setValue( urlParam == 'true' ? true : false );
      }
    }
  }
}

function saveAppParams( params ) {
  var state = {};
  var serialize = window.location.pathname + '?';
  var len = params.length;
  for( var i = 0; i < params.length; i++ ) {
    var param =  params[ i ];
    var key = param.key;
    state[ key ] = '' + param.getValue();
    if( param.type == 'number' ) {
      serialize += key + '=' + param.getValue() + '&';
    }
    else if( param.type == 'boolean' ) {
      serialize += key + '=' + ( param.getValue() ? 'true' : 'false' ) + '&';
    }
  }
  window.history.pushState( state, '', serialize );
}

function getUrlParams() {
  var query_string = {};
  var query = decodeURIComponent( decodeURIComponent( window.location.search.substring( 1 ) ) );
  query = query.split('+').join(' ');
  var vars = query.split( '&' );
  for( var i = 0; i < vars.length; i++ ) {
    var pair = vars[ i ].split( '=' );
    if (typeof query_string[ pair [ 0 ] ] === 'undefined') {
      query_string[ pair[ 0 ] ] = decodeURIComponent( pair[ 1 ] );
    } else if ( typeof query_string[ pair[ 0 ] ] === 'string' ) {
      var arr = [ query_string[ pair[ 0 ] ], decodeURIComponent( pair[ 1 ] ) ];
      query_string[ pair[ 0 ] ] = arr;
    } else {
      query_string[ pair[ 0 ] ].push( decodeURIComponent( pair[ 1 ] ) );
    }
  }
  return query_string;
}

window.onpopstate = function( event ) {
  loadAppParams( App.getParams(), event.state );
};

App.getCanvas().style.zIndex = 0;
document.body.style.backgroundColor = 'black';

var filename = '';
var textField = document.getElementById('filenamefield');
var textInputFn = function( event ) {
  filename = textField.value;
  var keyCode = event.keyCode || event.which;
  if ( keyCode === 13 ) {
    event.preventDefault();
    return false;
  }
};

textField.onkeyup = textInputFn;
textField.onkeypress = textInputFn;

function dataURItoBlob( data ) {
  var byteString;
  if( data.split( ',' )[ 0 ].indexOf( 'base64' ) >= 0 ) {
    byteString = atob( data.split( ',' )[ 1 ] );
  }
  else {
    byteString = unescape( data.split( ',' )[ 1 ] );
  }
  var mimeString = data.split( ',' )[ 0 ].split( ':' )[ 1 ].split( ';' )[ 0 ];
  var dataOut = new Uint8Array( byteString.length );
  for( var i = 0; i < byteString.length; i++ ) {
      dataOut[ i ] = byteString.charCodeAt( i );
  }
  return new Blob( [ dataOut ], { type : mimeString } );
}

function getShareOptions() {
  var width = 550;
  var height = 250;
  var winWidth = window.outerWidth;
  var winHeight = window.outerHeight;
  var sx = window.screenX;
  var sy = window.screenY;
  var xOffset = sx + ( winWidth - width ) * 0.5;
  var yOffset = sy + ( winHeight - height ) * 0.5;
  var options = 'scrollbars=yes,resizable=yes,toolbar=no,location=yes,';
  options += 'top=' + yOffset + ',';
  options += 'left=' + xOffset + ',';
  options += 'width=' + width + ',';
  options += 'height=' + height;
  return options;
}
