var Samsara = require('samsarajs');

var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;
var View = Samsara.Core.View;

var createSpacer = require('./createSpacer');

module.exports = View.extend( {
  defaults : {
    height: 56,
    widthPercent: 0.875,
    backgroundColor: 'rgba( 0, 0, 0, 0.5 )'
  },
  events : {
    open : "onOpen",
    close : "onClose"
  },
  initialize : function( options ) {
    this.easing = { curve: 'spring', damping: 0.75, period: 75, velocity: 0.0 };
    this.openState = new Transitionable( 0 );
    var translation = this.openState.map( function( value ) {
      return Transform.translateY( ( 1.0 - value ) * options.height );
    } );
    var opacity = this.openState.map( function( value ) { return value; } );

    var backgroundBar = new ContainerSurface( {
      origin: [ 0.5, 1 ],
      size: [ undefined, options.height ],
      proportions: [ options.widthPercent, undefined ],
      properties: {
        // margin:
        backgroundColor: options.backgroundColor
      }
    });

    var tfHtml = '<form action="#"><div class="mdl-textfield mdl-js-textfield"><input class="mdl-textfield__input" type="text" id="filenamefield"><label class="mdl-textfield__label" for="sample1">File Name</label></div></form>'

    var margin = [ 16, 10 ];
    var tfSize = 0.33;

    var textFieldContainer = new ContainerSurface( {
      origin: [ 0.0, 0.0 ],
      size: [ undefined, options.height ],
      proportions: [ tfSize, undefined ]
    } );

    var textFieldSurface = new Surface( {
      content: tfHtml,
      origin: [ 0.5, 0.95 ],
      size: [ undefined, options.height ],
      margins: [ margin[ 0 ] + 2, margin[ 1 ] ],
      properties: {
        color: 'rgba( 255, 255, 255, 0.75 )',
      }
    } );

    textFieldContainer.add( { align: [ 0.5, 0.5 ] } ).add( textFieldSurface );
    backgroundBar.add( textFieldContainer );

    var buttonContainer = new ContainerSurface( {
      origin: [ 1.0, 0.0 ],
      size: [ undefined, options.height ],
      proportions: [ 1.0 - tfSize, undefined ]
    } );

    backgroundBar.add( { align: [ 1.0, 0.0 ] } ).add( buttonContainer );

    var containerOptions = {
      proportions: [ 1.0 / 3.0, 1.0 ]
    };

    var buttonOptions = function( title ) {
      return {
        content: title,
        origin: [ 0.5, 0.5 ],
        size: [ undefined, options.height ],
        proportions: [ 0.85, 1.0 ],
        margins: [ 0.0, margin[ 1 ] ],
        properties: {
          color: 'rgba( 255, 255, 255, 0.75 )',
          lineHeight: ( options.height - margin[ 1 ] * 2.0 ) + 'px'
        },
        classes: [ 'mdl-button', 'mdl-js-button', 'mdl-js-ripple-effect' ]
      }
    };

    function createButton( title, that ) {
      var container = new ContainerSurface( containerOptions );
      var surface = new Surface( buttonOptions( title ) );
      surface.on( 'click', ( function() {
        this.emit( title.toLowerCase() );
      }).bind( that ) );
      container.add( { align: [ 0.5, 0.5 ] } ).add( surface );
      return container;
    }

    var buttons = [
      createButton( 'PNG', this ),
      createButton( 'OBJ', this ),
      createButton( 'STL', this )
    ];

    var len = buttons.length;
    for( var i = 0; i < len; i++ ) {
      var align = { align: [ i / len, 0.0 ] };
      buttonContainer.add( align ).add( createSpacer() );
      buttonContainer.add( align ).add( buttons[ i ] );
    }

    this.add( { align: [ 0.5, 1.0 ], transform: translation, opacity: opacity } ).add( backgroundBar );
  },

  onOpen : function( data ) {
    this.openState.set( 1.0, this.easing );
  },
  onClose : function( data ) {
    this.openState.set( 0.0, this.easing );
  }
} );
