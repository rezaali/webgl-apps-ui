var Samsara = require('samsarajs');

var MouseInput = Samsara.Inputs.MouseInput;
var Transform = Samsara.Core.Transform;
var Transitionable = Samsara.Core.Transitionable;
var Surface = Samsara.DOM.Surface;
var ContainerSurface = Samsara.DOM.ContainerSurface;
var Scrollview = Samsara.Layouts.Scrollview;

var View = Samsara.Core.View;

var Slider = require('./Slider');
var Toggle = require('./Toggle');
var Button = require('./Button');

module.exports = View.extend( {
  defaults : {
    params: undefined,
    margin: 16,
    heightOffset: 56,
    height: 56,
    width: 256,
    backgroundColor: 'rgba( 0, 0, 0, 0.5 )'
  },
  events : {
    open : "onOpen",
    close : "onClose",
    end: 'onEnd',
  },
  initialize : function( options ) {
    this.easing = { curve: 'spring', damping: 0.75, period: 50, velocity: 0.0 };
    this.openState = new Transitionable( 0 );
    var translation = this.openState.map( function( value ) {
      return Transform.translate( [ options.width * ( 1.0 - value ), 0.0, 0.0 ] );
    } );
    var opacity = this.openState.map( function( value ) { return value; } );

    var panel = new ContainerSurface( {
      origin: [ 1.0, 1.0 ],
      margins: [ 0.0, options.heightOffset * 0.5 ],
      size: [ options.width, undefined ],
      properties: {
        backgroundColor: options.backgroundColor,
        zIndex: 0,
      }
    });

    var scrollview = new Scrollview( {
        direction: Scrollview.DIRECTION.Y,
        drag: 0.3
    } );

    var titleContainer = new ContainerSurface( {
      size: [ undefined, options.height ]
    } );

    var title = new Surface( {
      origin: [ 0.5, 0.5 ],
      content: 'Parameters',
      margins: [ options.margin, 0 ],
      properties: {
        fontFamily: 'Roboto',
        fontWeight: 200,
        color: 'white',
        fontSize: '20px',
        lineHeight: ( options.height + 'px' )
      },
      classes: [ 'noselect' ]
    } );

    function createSpacer() {
      return new Surface( {
         origin: [ 0.5, 0.5 ],
         size: [ undefined, 0.5 ],
         margins: [ options.margin, 0 ],
         properties: {
           backgroundColor: 'rgba( 255, 255, 255, 0.1 )',
         }
      } );
    }

    titleContainer.add( { align: [ 0.5, 1.0 ] } ).add( createSpacer() );
    titleContainer.add( { align: [ 0.5, 0.5 ] } ).add( title );

    var elements = [ titleContainer ];
    var params = options.params;
    var plen = params.length;
    for( var i = 0; i < plen; i++ ) {
      var param = params[ i ];
      var container = new ContainerSurface( { size: [ undefined, options.height ] } );
      var element = undefined;

      if( param.type == 'number') {
        element = createSlider( param, { width: options.width - options.margin * 2.0, height: options.height } );
      }
      else if( param.type == 'boolean' ) {
        element = createToggle( param, { width: options.width - options.margin * 2.0, height: options.height } );
      }
      else if( param.type == 'button' ) {
        element = createButton( param, { width: options.width - options.margin * 2.0, height: options.height } );
      }

      if( element ) {
        this.subscribe( element );
        container.add( { align: [ 0.5, 0.0 ] } ).add( element );
        container.add( { align: [ 0.5, 0.0 ] } ).add( createSpacer() );
        elements.push( container );
      }
    }
    scrollview.addItems( elements );
    panel.add( scrollview );
    this.add( { align: [ 1.0, 1.0 ], transform: translation, opacity: opacity } ).add( panel );
  },
  onOpen : function( data ) {
    this.openState.set( 1.0, this.easing );
  },
  onClose : function( data ) {
    this.openState.set( 0.0, this.easing );
  },
  onEnd : function( data ) {
    this.emit( 'data', data );
  }
} );

function createSlider( param, options ) {
  var slider = new Slider( {
    param: param,
    width: options.width,
    height: options.height,
  } );
  return slider;
}

function createToggle( param, options ) {
  var toggle = new Toggle( {
    param: param,
    label: { content: param.key },
    width: options.width,
    height: options.height,
  } );
  return toggle;
}

function createButton( param, options ) {
  var button = new Button( {
    param: param,
    label: { content: param.key },
    width: options.width,
    height: options.height,
  } );
  return button;
}
